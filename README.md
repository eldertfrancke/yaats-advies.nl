# Website yaats-advies.nl
## To do
- Migrate from custom build script to the webpack system.
- Help section for end-users.

## Development
- Run `npm run dev` to start the **lite-server**.
- Run `gulp` or `npm run build` to build the app from `src` to `build`.
