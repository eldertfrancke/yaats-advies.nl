var del = require('del')
var gulp = require('gulp')
var scan = require('gulp-scan')
var concat = require('gulp-concat')
// var concatCss = require('gulp-concat-css')
var deleteLines = require('gulp-delete-lines')
var insertLines = require('gulp-insert-lines')
// var put = require('gulp-put')
const path = require('path')

var cssFiles = []
var jsFiles = []

const buildDir = 'app/public'

gulp.task('del:build', function () {
  return del([
    // 'app/public/**/*',
    path.join(buildDir, '**/*')
  ])
})

gulp.task('build:images', ['del:build'], function () {
  gulp.src('src/img/**')
  // .pipe(gulp.dest('build/img'));
  .pipe(gulp.dest(path.join(buildDir, 'img')))
})

// Find javascript node_modules in index.html
// Add references to global modules array
gulp.task('scan:node_modules', function () {
  return gulp.src('src/index.html')
  .pipe(scan({ term: /(\/node_modules\/)(.+)(\.js)/g,
    fn: function (match, file) {
    // Make path relative
      var dot = '.'
      match = dot.concat(match)

    // Replace .js with .min.js
    // FIXME: taking minified files gives error because map files are seperated.
    // SOLUTION_1: do not use minified script and minify in gulp script
    // SOLUTION_2: include .map-files
      match = match.replace('js', 'min.js')

    // Add item to array jsFiles
      jsFiles.push(match)
    }}))
})

gulp.task('scan:css_files', function () {
  return gulp.src('src/index.html')
  .pipe(scan({ term: /<link\srel=["|']stylesheet["|'].+>\n/g,
    fn: function (result, file) {
    // Make path relative
      var prefix = './src'

      var regex = /href=['|"](.)+\.css['|"]/g
      var ref = result.match(regex)

      var cssFile = prefix.concat(ref[0].split('"')[1])
      cssFile = cssFile.replace('.css', '.min.css')

    // Add item to array jsFiles
      cssFiles.push(cssFile)
    }}))
})

gulp.task('concat:css', ['del:build', 'scan:css_files'], function () {
  return gulp.src(cssFiles)
  .pipe(concat('bundled.min.css'))
  .pipe(gulp.dest(buildDir))
})

gulp.task('concat:js', ['del:build', 'scan:node_modules'], function () {
  return gulp.src(jsFiles)
  .pipe(concat('bundled.min.js'))
  .pipe(gulp.dest(buildDir))
})

gulp.task('modify:html', ['del:build', 'scan:node_modules', 'scan:css_files'], function () {
  // Delete references to node_modules (js and css)
  gulp.src('./src/index.html')
  .pipe(deleteLines({
    'filters': [
      /node_modules/
    ]
  }))
  .pipe(deleteLines({
    'filters': [
      /<link rel=["|']stylesheet["|']/
    ]
  }))
  // Insert reference to bundled javascript modules
  .pipe(insertLines({
    'before': /<script src="\/app.js" charset="utf-8"><\/script>/g,
    'lineBefore': '<script type="text/javascript" src="bundled.min.js"></script>'
  }))
  // // Todo: insert reference to bundled css file
  .pipe(insertLines({
    'before': /<title>.+<\/title>/,
    'lineBefore': '<link rel="stylesheet" href="bundled.min.css">'
  }))
  .pipe(gulp.dest(buildDir))
})

gulp.task('build:app', ['del:build'], function () {
  gulp.src('./src/app.js')
  // TODO: UGLIFY JS
  .pipe(gulp.dest(buildDir))
})

gulp.task('default', [
  'del:build',
  'build:images',
  'scan:node_modules',
  'scan:css_files',
  'concat:js',
  'concat:css',
  'modify:html',
  'build:app'
], function () {

})
