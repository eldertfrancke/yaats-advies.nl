//npm i webpack babel-core babel-loader babel-preset-es2015 html-loader css-loader ng-annotate-loader extract-text-webpack-plugin -D
var webpack = require('webpack');
// https://github.com/webpack/webpack/tree/master/examples/css-bundle
var ExtractTextPlugin = require('extract-text-webpack-plugin');
module.exports = {
  entry: {
    index: './src/index.js',
  },
  output: {
    path: './app/public',
    filename: '[name].bundle.js',
  },
  module: {
    loaders: [
        {
          test: /client.*\.js$/,
          loaders: ['ng-annotate', 'babel-loader?presets[]=es2015'],
        },
        { test: /client.*\.js$/, loader: 'babel' },
        { test: /client.*\.html$/, loaders: ['html'] },
        { test: /\.css$/, loader: ExtractTextPlugin.extract('css-loader') },
    ],
  },
  plugins: [
    new ExtractTextPlugin('[name].bundle.css', { allChunks: true }),
  ],
};
