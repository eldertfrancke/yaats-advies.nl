angular.module('app', ['ngMaterial'])
.config(function($mdThemingProvider){
  $mdThemingProvider.theme('default')
  .primaryPalette('indigo')
  .accentPalette('pink')
  .warnPalette('red')
  .backgroundPalette('grey');
})
.service('DienstenService', function() {
  var diensten = {};
  diensten.voorrijkosten = {};
  // Kosten voor enkele reis is 30 cent per km.
  diensten.voorrijkosten.kmPrijs = 0.3;
  diensten.voorrijkosten.veelvoud = 10;
  diensten.voetnoot = 'Inclusief BTW, exclusief (eventuele) voorrijkosten. Prijs per computersysteem.';
  diensten.onderhoud = {};
  diensten.onderhoud.prijs = '€60 *';
  diensten.onderhoud.onderdelen = [
    'Controle op fouten in hard- en software.',
    'Een grondige anti-viruscheck en verwijdering van ongewenste software.',
    'Opschonen van overbodige bestanden en instellingen.',
    'Bijwerken van verouderde software',
    'Check op mogelijk misbruik van uw inloggegevens.',
    'Een rapportage met verslag van werkzaamheden en aanbevelingen.',
    'Een overzicht van uw licenties, support- en onderhoudscontracten.',
  ];

  diensten.hulpOpAfstand = {};
  diensten.hulpOpAfstand.prijs = '€15 per kwartier *';
  diensten.hulpOpAfstand.voetnoot = 'Inclusief BTW.';
  diensten.hulpOpAfstand.onderdelen = [
    'Installatie van een hulp-op-afstandprogramma.',
    'Geschikt voor zowel Windows, Mac en Linux.',
    'Eenvoudig, veilig en vlot.',
  ];

  diensten.backup = {};
  diensten.backup.prijs = '€60 *';
  // diensten.backup.voetnoot = '';
  diensten.backup.onderdelen = [
    'Installatie en uitleg van het back-up-programma.',
    'Inclusief disaster-recovery-cd.',
    'Mogelijkheid van terugzetten van losse bestanden of zelfs een compleet systeem.',
    'Biedt bescherming tegen de gevolgen van ransomware.',
    'Vereist een externe harde schijf (circa €80).'
    ];

  return diensten;
}).
controller('MainCtrl', function(DienstenService) {
  var that = this;
  that.voorrijkosten = {};

  this.diensten = DienstenService;

  this.voorrijkostenCalculator = function(kmEnkeleReis) {
    // voorrijkosten rekenent met een retourafstand
    var voorrijkosten = kmEnkeleReis * 2 * DienstenService.voorrijkosten.kmPrijs;
    // Rond de kosten af naar beneden naar een ingesteld veelvoud
    var voorrijkostenAfgerond = Math.floor(voorrijkosten/DienstenService.voorrijkosten.veelvoud)*DienstenService.voorrijkosten.veelvoud;
    that.voorrijkosten.kosten = voorrijkosten.toFixed(0);
    that.voorrijkosten.afgerond = voorrijkostenAfgerond;
    that.voorrijkosten.korting = voorrijkosten.toFixed(0) - voorrijkostenAfgerond;
  };
});
